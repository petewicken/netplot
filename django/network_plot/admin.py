from django.contrib import admin

from network_plot import models as np_models


class ConnectionAdmin(admin.ModelAdmin):
    list_display = ['pk', 'source', 'destination', 'connection_type']
admin.site.register(np_models.Connection, ConnectionAdmin)


class ConnectionTypeAdmin(admin.ModelAdmin):
    list_display = ['name', 'category']
admin.site.register(np_models.ConnectionType, ConnectionTypeAdmin)


class ConnectionCategoryAdmin(admin.ModelAdmin):
    list_display = ['name']
admin.site.register(np_models.ConnectionCategory, ConnectionCategoryAdmin)


class DeviceAdmin(admin.ModelAdmin):
    list_display = ['hostname', 'mac_address', 'ip_address', 'device_type', 'note']
admin.site.register(np_models.Device, DeviceAdmin)


class DeviceTypeAdmin(admin.ModelAdmin):
    list_display = ['name']
admin.site.register(np_models.DeviceType, DeviceTypeAdmin)


class NodeAdmin(admin.ModelAdmin):
    list_display = ['hostname', 'note']
admin.site.register(np_models.Node, NodeAdmin)
