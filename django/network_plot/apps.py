from django.apps import AppConfig


class NetworkPlotConfig(AppConfig):
    name = 'network_plot'
    verbose_name = 'NetPlot'
