from django.contrib.auth.models import User, Group
from rest_framework import serializers

from network_plot import models as nw_models


class UserSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = User
        fields = ('url', 'username', 'email', 'groups')


class GroupSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = Group
        fields = ('url', 'name')


class NodeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = nw_models.ConnectionType
        fields = ('url', 'hostname', 'note')


class DeviceTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = nw_models.DeviceType
        fields = ('url', 'name',)


class DeviceSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = nw_models.Device
        fields = ('url', 'mac_address', 'ip_address', 'device_type')


class ConnectionCategorySerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = nw_models.ConnectionType
        fields = ('url', 'name',)


class ConnectionTypeSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = nw_models.ConnectionType
        fields = ('url', 'name', 'category')


class ConnectionSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = nw_models.Connection
        fields = ('url', 'source', 'destination', 'duplex', 'connection_type')
