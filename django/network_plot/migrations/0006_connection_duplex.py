# -*- coding: utf-8 -*-
# Generated by Django 1.9 on 2016-01-02 12:17
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('network_plot', '0005_auto_20160102_1116'),
    ]

    operations = [
        migrations.AddField(
            model_name='connection',
            name='duplex',
            field=models.CharField(choices=[(b'FULL', b'FULL'), (b'HALF', b'HALF')], default='FULL', max_length=4),
            preserve_default=False,
        ),
    ]
