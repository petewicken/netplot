from django.db import models


class Node(models.Model):
    hostname = models.CharField(max_length=30)
    note = models.TextField(blank=True)

    def __str__(self):
        return str(self.hostname)


class DeviceType(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.name)


class Device(Node):
    mac_address = models.CharField(max_length=17, blank=True)
    ip_address = models.GenericIPAddressField(max_length=17,
                                              blank=True,
                                              null=True)
    device_type = models.ForeignKey('DeviceType', null=True)


class ConnectionCategory(models.Model):
    name = models.CharField(max_length=30)

    def __str__(self):
        return str(self.name)


class ConnectionType(models.Model):
    name = models.CharField(max_length=30)
    category = models.ForeignKey('ConnectionCategory')

    def __str__(self):
        return '{0} ({1})'.format(self.name, self.category)


class Connection(models.Model):
    DUPLEX_CHOICES = (
        ('FULL', 'FULL'),
        ('HALF', 'HALF'),
    )

    source = models.ForeignKey('Node', related_name='source')
    destination = models.ForeignKey('Node', related_name='destination')
    duplex = models.CharField(max_length=4, choices=DUPLEX_CHOICES)
    connection_type = models.ForeignKey('ConnectionType')

    def __str__(self):
        return '{0} --> {1}'.format(self.source, self.destination)
