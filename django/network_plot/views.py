from rest_framework import viewsets
from django.contrib.auth.models import User, Group

from network_plot import models as nw_models
from network_plot import serializers as nw_serializers


class UserViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = User.objects.all().order_by('-date_joined')
    serializer_class = nw_serializers.UserSerializer


class GroupViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows groups to be viewed or edited.
    """
    queryset = Group.objects.all()
    serializer_class = nw_serializers.GroupSerializer


class ConnectionViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = nw_models.Connection.objects.all()
    serializer_class = nw_serializers.ConnectionSerializer


class ConnectionTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = nw_models.ConnectionType.objects.all()
    serializer_class = nw_serializers.ConnectionTypeSerializer


class ConnectionCategoryViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = nw_models.ConnectionCategory.objects.all()
    serializer_class = nw_serializers.ConnectionCategorySerializer


class DeviceViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = nw_models.Device.objects.all()
    serializer_class = nw_serializers.DeviceSerializer


class DeviceTypeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = nw_models.DeviceType.objects.all()
    serializer_class = nw_serializers.DeviceTypeSerializer


class NodeViewSet(viewsets.ModelViewSet):
    """
    API endpoint that allows users to be viewed or edited.
    """
    queryset = nw_models.Node.objects.all()
    serializer_class = nw_serializers.NodeSerializer
