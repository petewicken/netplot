from django.db import models


class Node(models.Model):
    name = models.CharField(max_length=30)
    note = models.TextField(blank=True)


class DeviceType(models.Model):
    name = models.CharField(max_length=30)


class Device(Node):
    mac_address = models.CharField(max_length=17, blank=True)

    def __str__(self):
        return str(self.name)


class ConnectionType(models.Model):
    name = models.CharField(max_length=30)


class Connection(models.Model):
    source = models.ForeignKey('Node', related_name='source')
    destination = models.ForeignKey('Node', related_name='destination')
    connection_type = models.ForeignKey('ConnectionType')
